﻿Feature: Menu
	Click on TEQUILA in PRODUCTS menu of https://skywalk.info/

@menu
Scenario: Perform click on menu item
	Given open home page
	And hover PRODUCTS menu
	When click on TEQUILA
	Then check opened url and check text