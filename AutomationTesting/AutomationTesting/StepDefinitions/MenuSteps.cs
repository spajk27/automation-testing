﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using TechTalk.SpecFlow;

namespace AutomationTesting.StepDefinitions
{
    [Binding]
    public class MenuSteps
    {
        ChromeDriver chromeDriver;

        public MenuSteps() => chromeDriver = new ChromeDriver();

        [Given(@"open home page")]
        public void GivenOpenHomePage()
        {
            chromeDriver.Navigate().GoToUrl("https://skywalk.info/");
        }
        
        [Given(@"hover PRODUCTS menu")]
        public void GivenHoverPRODUCTSMenu()
        {
            var element = chromeDriver.FindElementByLinkText("PRODUCTS");
            Actions builder = new Actions(chromeDriver);
            builder.MoveToElement(element).Perform();
        }
        
        [When(@"click on TEQUILA")]
        public void WhenClickOnTEQUILA()
        {
            chromeDriver.FindElementByLinkText("TEQUILA").Click();
        }
        
        [Then(@"check opened url and check text")]
        public void ThenCheckOpenedUrlAndCheckText()
        {
            Assert.IsTrue(chromeDriver.Url.ToLower().Contains("tequila"));
            Assert.AreEqual("Free your mind. For the essentials.", chromeDriver.FindElementByXPath("/html/body/div[1]/div[2]/div/article/div/div/div/div/div[2]/div/div[1]/div[1]/div/h1").Text);
            chromeDriver.Close();
        }
    }
}
