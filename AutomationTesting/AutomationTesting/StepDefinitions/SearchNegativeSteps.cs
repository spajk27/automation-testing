﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using TechTalk.SpecFlow;

namespace AutomationTesting.StepDefinitions
{
    [Binding]
    public class SearchNegativeSteps
    {
        ChromeDriver chromeDriver;

        public SearchNegativeSteps() => chromeDriver = new ChromeDriver();

        [Given(@"open home page third time")]
        public void GivenOpenHomePageThirdTime()
        {
            chromeDriver.Navigate().GoToUrl("https://skywalk.info/");
        }
        
        [Given(@"type chily in search field")]
        public void GivenTypeChilyInSearchField()
        {
            var element = chromeDriver.FindElementByXPath("/html/body/div/header/div[1]/div[2]/div[1]/span");
            Actions builder = new Actions(chromeDriver);
            builder.Click(element).Perform();
            chromeDriver.FindElementByXPath("/html/body/div[1]/header/div[2]/div/form/input").SendKeys("chily");
        }
        
        [When(@"perform search for chily")]
        public void WhenPerformSearchForChily()
        {
            chromeDriver.FindElementByXPath("/html/body/div[1]/header/div[2]/div/form/input").Submit();
        }
        
        [Then(@"check opened page url and check error text")]
        public void ThenCheckOpenedPageUrlAndCheckErrorText()
        {
            Assert.IsTrue(chromeDriver.Url.ToLower().Contains("chily"));
            Assert.AreEqual("No Results Found", chromeDriver.FindElementByXPath("/html/body/div/div[2]/div/div/div/div[1]/div/h1").Text);
            chromeDriver.Close();
        }
    }
}
