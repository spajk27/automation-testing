﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using TechTalk.SpecFlow;

namespace AutomationTesting.StepDefinitions
{
    [Binding]
    public class SearchPositiveSteps
    {
        ChromeDriver chromeDriver;

        public SearchPositiveSteps() => chromeDriver = new ChromeDriver();

        [Given(@"open home page again")]
        public void GivenOpenHomePageAgain()
        {
            chromeDriver.Navigate().GoToUrl("https://skywalk.info/");
        }
        
        [Given(@"type chili in search field")]
        public void GivenTypeChiliInSearchField()
        {
            var element = chromeDriver.FindElementByXPath("/html/body/div/header/div[1]/div[2]/div[1]/span");
            Actions builder = new Actions(chromeDriver);
            builder.Click(element).Perform();
            chromeDriver.FindElementByXPath("/html/body/div[1]/header/div[2]/div/form/input").SendKeys("chili");
        }

        [When(@"perform search")]
        public void WhenPerformSearch()
        {
            chromeDriver.FindElementByXPath("/html/body/div[1]/header/div[2]/div/form/input").Submit();
        }
        
        [Then(@"check opened page url and check text")]
        public void ThenCheckOpenedPageUrlAndCheckText()
        {
            Assert.IsTrue(chromeDriver.Url.ToLower().Contains("chili"));
            Assert.AreEqual("CHILI4 – Limited Design – “Yellow”", chromeDriver.FindElementByXPath("/html/body/div/div[2]/div/div/div/div[1]/article[1]/h2/a").Text);
            chromeDriver.Close();
        }
    }
}
